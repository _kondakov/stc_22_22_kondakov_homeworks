public class Main {
    public static void main(String[] args) {

        ProductsRepository productsRepository = new ProductsRepositoryFileBasedImpl("Products.txt");

        System.out.println(productsRepository.findById(7).toString());
        System.out.println(productsRepository.findAllByTitleLike("Хурма"));
    }
}