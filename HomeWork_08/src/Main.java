public class Main {
    public static void main(String[] args) {
        ArrayTask sumNum = (array, from, to) -> {
            int sum = 0;
            for (int i = from; i < to; i++) {
                sum += array[i];
            }
            return sum;
        };

        ArrayTask maxDigitSum = (array, from, to) -> {
            int max = 0;
            for (int i = from; i < to; i++) {
                if (max < array[i]) {
                    max = array[i];
                }
            }
            int sum = 0;
            while (max > 0) {
                sum += max % 10;
                max = max / 10;
            }
            return sum;
        };

        int[] array = {5, 23, 735, 234, 35, 68, 345, 657};
        ArraysTasksResolver.resolveTask(array, maxDigitSum, 2, 5);
        ArraysTasksResolver.resolveTask(array, sumNum, 2, 5);
    }
}
