import java.util.Arrays;

public class ArraysTasksResolver {

    public static void resolveTask(int[] array, ArrayTask task, int from, int to) {
        System.out.println(Arrays.toString(array));
        System.out.println("From: " + from + ", To: " + to);
        System.out.println(task.resolve(array, from, to));
    }
}
