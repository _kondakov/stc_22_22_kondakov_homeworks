public class Bankomat {
    int balance = 1000;
    int maxGiveMoney = 10000;
    int maxBalance = 500000;
    int sumOperations = 0;

    public int withDraw(int sumWithDraw) {
        this.sumOperations++;
        if (sumWithDraw <= balance && sumWithDraw <= maxGiveMoney) {
            this.balance -= sumWithDraw;
            return sumWithDraw;
        } else {
            System.out.println("Денег не достаточно");
            return 0;
        }

    }

    public int toDeposit(int sumDeposit) {
        this.sumOperations++;
        int returnDeposit;
        if ((sumDeposit + balance) <= maxBalance) {
            this.balance += sumDeposit;
            return balance;
        } else {
            returnDeposit = (sumDeposit + balance) - maxBalance;
            return returnDeposit;
        }
    }
}
