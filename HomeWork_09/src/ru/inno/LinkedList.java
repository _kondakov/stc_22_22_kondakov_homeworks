package ru.inno;

public class LinkedList<T> implements List<T> {

    private Node<T> first;

    private Node<T> last;
    private int count;

    private static class Node<E> {
        E value;
        Node<E> next;

        public Node(E value) {
            this.value = value;
        }
    }

//    @Override
//    public void add(T element) {
//        // создаем новый узел со значением
//        Node<T> newNode = new Node<>(element);
//        // если элементов в списке нет
//        if (count == 0) {
//            // кладем новый узел в качестве первого
//            this.first = newNode;
//        } else {
//            // если в списке уже есть узлы, нужно добраться до последнего
//
//            // запоминаем ссылку на первый узел
//            Node<T> current = this.first;
//            // пока не долшли до узла, у которого нет следующего узла (до последнего)
//            while (current.next != null) {
//                current = current.next;
//            }
//            // теперь у нас current указывает на последний узел
//
//            // делаем следующим после последнего - новый узел
//            current.next = newNode;
//        }
//        count++;
//    }

    @Override
    public void add(T element) {
        Node<T> newNode = new Node<>(element);
        if (count == 0) {
            this.first = newNode;
        } else {
            this.last.next = newNode;
        }
        this.last = newNode;
        count++;
    }

    @Override
    public void remove(T element) {

    }

    @Override
    public boolean contains(T element) {
        Node<T> current = this.first;
        while (current != null) {
            if (current.value.equals(element)) {
                return true;
            }
            current = current.next;
        }
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeAt(int index) {

    }

    @Override
    public T get(int index) {
        if (0 <= index && index <= count) {
            Node<T> current = this.first;
            for (int i = 0; i < index; i++) {
                current = current.next;
            }
            return current.value;
        }
        return null;

    }

    private class LinkedListIterator implements Iterator<T> {

        private Node<T> current = first;

        @Override
        public T next() {
            T value = current.value;
            current = current.next;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current != null;
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new LinkedListIterator();
    }
}
