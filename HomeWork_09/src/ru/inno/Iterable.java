package ru.inno;

/**
 * Интерфейс, который показывает, что какой-либо тип (коллекция) может быть
 * проитерирован
 * @param <T> - тип элементов коллекций
 */
public interface Iterable<T> {
    Iterator<T> iterator();
}