import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Размер массива: " );
        int lengtharray = sc.nextInt();
        int[] array = new int[lengtharray];
        for (int i = 0; i < lengtharray; i++) {
            array[i] = sc.nextInt();
        }
        System.out.println(Arrays.toString(array));
        int sumnum = Sum (array, 0, 3);
        System.out.println(sumnum);
    }

    public static int Sum(int[] a, int from, int to) {
        int sum = 0;
        if (from > to) {
            return -1;
        }
        for (int i = from; i <= to; i++) {
            sum += a[i];
        }
        return sum;
    }
}
