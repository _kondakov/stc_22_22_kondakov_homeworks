public class Main {

    public static void main(String[] args) {
        Task x = new EvenNumbersPrintTask(3, 20);
        Task y = new OddNumbersPrintTask(5, 12);
        Task[] tasks = {x, y};
        completeAllTasks(tasks);
    }

    public static void completeAllTasks(Task[] tasks) {
        for (Task task : tasks) {
            task.complete();
        }
    }
}
