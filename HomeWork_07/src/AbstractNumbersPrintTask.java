import java.util.Scanner;

public abstract class AbstractNumbersPrintTask implements Task {
    int from;
    int to;

    public AbstractNumbersPrintTask(int from, int to) {
        this.from = from;
        this.to = to;
    }

    @Override
    public void complete() {
        for (int i = from; i < to; i++) {
            System.out.println(i);
        }
    }
}
