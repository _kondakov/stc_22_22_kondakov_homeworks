insert into account (first_name, last_name, phone_number, experience, rating, rights_category, age, driver_license)
values ('Виталий','Жирнов', 89002321210, '2 years', 4, 'B,C', 20, true);
insert into account (first_name, last_name, phone_number, experience, rating, rights_category, age, driver_license)
values ('Ахмет','Сунгатуллин', 89053156478, '15 years', 2, 'A,B,C,D,E', 39, true);
insert into account (first_name, last_name, phone_number, experience, rating, rights_category, age, driver_license)
values ('Мурат','Ахмеров', 89093567765, '25 years', 1, 'A,B,C,D,E,M', 59, true);
insert into account (first_name, last_name, phone_number, experience, rating, rights_category, age, driver_license)
values ('Владимир','Кулагин', 89061123212, '9 years', 5, 'A,B,C', 29, true);
insert into account (first_name, last_name, phone_number, experience, rating, rights_category, age, driver_license)
values ('Евгений','Корнилов', 89274123214, '20 years', 2, 'A,B,C,D,E', 69, true);
insert into account (first_name, last_name, phone_number, experience, rating, rights_category, age, driver_license)
values ('Ильнур','Гимадеев', 89093120001, '12 years', 1, 'A,B,C,D,E', 33, true);

insert into car (model, color_car, number_car,owner_id)
values ('Camry','Black', 'A293AA116RUS',1),
       ('Rav4','White','O199PM777RUS',3),
       ('LC200','Grey','O777OO116RUS',5),
       ('Rav4','Black','A990OO178RUS',4),
       ('Corolla','Blue','M007OA199RUS',2);

insert into trip (driver_id, car_id, date_trip, duration_trip)
values (5,1,'2022-11-07','01:02:00'),
       (4,2,'2022-11-10','04:12:00'),
       (3,3,'2022-11-05','01:42:00'),
       (2,4,'2022-11-06','03:32:00'),
       (1,5,'2022-11-08','02:15:01')
;

