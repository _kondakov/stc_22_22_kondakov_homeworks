drop table if exists account;
drop table if exists car;
drop table if exists trip;

create table account (
                         id_account bigserial primary key,
                         first_name varchar not null,
                         last_name varchar not null,
                         phone_number varchar not null,
                         experience varchar not null,
                         rating integer not null,
                         rights_category varchar not null,
                         age integer check (age >= 0 and age <= 70) not null,
                         driver_license bool not null
);

create table car (
                     id_cars bigserial primary key,
                     model varchar not null,
                     color_car varchar not null,
                     number_car varchar not null,
                     owner_id integer not null
);

create table trip (
                      id_trip bigserial primary key,
                      driver_id integer not null,
                      car_id integer not null,
                      date_trip date not null,
                      duration_trip time not null
);

alter table car
    add constraint car_foreign_key
        foreign key (owner_id)
            references account(id_account)
;

alter table trip
    add constraint trip_foreign_key
        foreign key (driver_id)
            references account (id_account),
    add constraint car_foreign_key
    foreign key (car_id)
    references car (id_cars)
;
