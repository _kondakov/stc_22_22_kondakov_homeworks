insert into account (first_name, last_name, phone_number, experience, rating, rights_category, age, driver_license)
values ('Александр','Кондаков', 89613613131, '12 years', 4, 'B', 36, true);
insert into account (first_name, last_name, phone_number, experience, rating, rights_category, age, driver_license)
values ('Армен','Салмян', 89053435765, '15 years', 2, 'A,B,C,D,E', 39, true);
insert into account (first_name, last_name, phone_number, experience, rating, rights_category, age, driver_license)
values ('Марат','Димаев', 89272457243, '11 years', 1, 'A,B,C,D', 34, true);
insert into account (first_name, last_name, phone_number, experience, rating, rights_category, age, driver_license)
values ('Владимир','Юдин', 89061334455, '9 years', 5, 'A,B,C', 42, true);
insert into account (first_name, last_name, phone_number, experience, rating, rights_category, age, driver_license)
values ('Евгений','Таранков', 89270982234, '20 years', 2, 'A,B,C', 69, true);


insert into car (model, color_car, number_car,owner_id)
values ('Camry','Black', 'A777AA777RUS',1),
       ('Corolla','White','O112OM777RUS',3),
       ('LC200','Black','X777XX163RUS',5),
       ('Prado','Black','A323XA163RUS',4),
       ('Corolla','Red','T001TM199RUS',2);

insert into trip (driver_id, car_id, date_trip, duration_trip)
values (5,1,'2022-11-23','01:00:00'),
       (4,2,'2022-11-29','06:30:00'),
       (3,3,'2022-11-25','08:45:00'),
       (2,4,'2022-11-26','03:30:00'),
       (1,5,'2022-11-28','02:00:00')
;

