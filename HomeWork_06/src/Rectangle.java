public class Rectangle extends Square {
    private int sideB;

    public Rectangle(int centerA, int centerB, int sideA, int sideB) {
        super(centerA, centerB, sideA);
        this.sideB = sideB;
    }

    @Override
    public void move(int toX, int toY) {
        super.move(toX, toY);
    }

    public int area(int sideA, int sideB) {
        return sideA * sideB;
    }

    public int perimeter(int sideA, int sideB) {
        return 2 * (sideA + sideB);
    }
}
