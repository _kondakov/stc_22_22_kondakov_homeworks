public class Main {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(0, 0, 10, 15);
        Square square = new Square(0, 0, 8);
        Circle circle = new Circle(0, 0, 12);
        Ellipse ellipse = new Ellipse(0, 0, 12, 18);

        rectangle.move(22, 35);
        square.move(3, 5);
        circle.move(2, 48);
        ellipse.move(27, 3);

        System.out.println(square.area(5));
        System.out.println(rectangle.area(12, 3));
        System.out.println(circle.area(6));
        System.out.println(ellipse.area(7));

        System.out.println(square.perimeter(5));
        System.out.println(rectangle.perimeter(12, 3));
        System.out.println(circle.perimeter(6));
        System.out.println(ellipse.perimeter(7));
    }
}
