public abstract class Figure {
    private int centerA;
    private int centerB;

    public void move(int toX, int toY) {
        this.centerA = toX;
        this.centerB = toY;
        System.out.println("Новые координаты: " + toX + ", " + toY);
    }

    public Figure(int centerA, int centerB) {
        this.centerA = centerA;
        this.centerB = centerB;
    }

    public int getCenterA() {
        return centerA;
    }

    public int getCenterB() {
        return centerB;
    }
}
