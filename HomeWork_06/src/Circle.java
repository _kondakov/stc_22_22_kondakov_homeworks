public class Circle extends Figure {
    private int radius;

    public Circle(int centerA, int centerB, int radius) {
        super(centerA, centerB);
        this.radius = radius;
    }

    @Override
    public void move(int toX, int toY) {
        super.move(toX, toY);
    }

    public double area(int radius) {
        return (Math.PI * (radius * radius));
    }

    public double perimeter(int radius) {
        return (2 * radius * Math.PI);
    }
}
