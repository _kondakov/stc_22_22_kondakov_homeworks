public class Square extends Figure {
    private int sideA;

    public Square (int centerA, int centerB, int sideA) {
        super(centerA, centerB);
        this.sideA = sideA;
    }

    @Override
    public void move(int toX, int toY) {
        super.move(toX, toY);
    }

    public int area(int sideA) {
        return sideA * sideA;

    }

    public int perimeter(int sideA) {
        return sideA * 4;
    }
}
