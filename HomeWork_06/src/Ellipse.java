import static java.lang.Math.*;

public class Ellipse extends Circle {
    private int bigR;

    public Ellipse(int centerA, int centerB, int radius, int bigR) {
        super(centerA, centerB, radius);
        this.bigR = bigR;
    }

    @Override
    public void move(int toX, int toY) {
        super.move(toX, toY);
    }

    public double area(int radius, int bigR) {
        return (PI * (radius * bigR));
    }

    public double perimeter(int radius, int bigR) {
        double a = (((radius * radius) + (bigR * bigR)) / 2);
        return 2 * Math.PI * (sqrt(a));
    }
}
